export const initialState = {
  counter: 0,
}

export const reducers = (state = initialState, action: any) => {
  switch (action.type) {
    default:
      return state
  }
}

export type RootState = typeof initialState
