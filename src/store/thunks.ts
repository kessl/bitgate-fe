import { RootThunk } from '.'
import { Job, AddJobFormData } from '../data/types'

export const fetchQueue = (): RootThunk<Job[]> => async (dispatch, getState, { bridge }) => {
  return bridge.getQueue()
}

export const addJob = (data: AddJobFormData): RootThunk<Job> => async (
  dispatch,
  getState,
  { bridge }
) => {
  return bridge.insertJob(data)
}

export const fetchHistory = (): RootThunk<Job[]> => async (dispatch, getState, { bridge }) => {
  return bridge.getHistory()
}
