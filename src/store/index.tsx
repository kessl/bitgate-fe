import React, { useCallback } from 'react'
import { Provider, TypedUseSelectorHook, useSelector, useDispatch } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { reducers, RootState, initialState } from './reducers'
import { BridgeClient } from '../data/BridgeClient'

export * from './actions'
export * from './reducers'
export * from './thunks'

export interface RootClients {
  bridge: BridgeClient
}

export type RootThunk<TRet = unknown> = (
  dispatch: <T>(action: T) => Promise<T> | T,
  getState: () => RootState,
  clients: RootClients
) => Promise<TRet>

export const useRootSelector: TypedUseSelectorHook<RootState> = useSelector
export function useRootDispatch<T extends (...args: any[]) => RootThunk | any>(
  callback: T
): (...args: Parameters<T>) => ReturnType<ReturnType<T>> {
  const dispatch = useDispatch()
  const creator = useCallback((...args) => dispatch(callback(...args)), [callback, dispatch])

  return creator
}

const clients: Partial<RootClients> = {
  bridge: new BridgeClient({
    baseUrl: process.env.API_BASE_URL,
  }),
}

export const store = createStore(
  reducers,
  initialState,
  applyMiddleware(thunk.withExtraArgument(clients))
)

export const withRedux = (Component: React.ComponentType, reduxStore = store) =>
  class WithRedux extends React.Component {
    render() {
      return (
        <Provider store={reduxStore}>
          <Component />
        </Provider>
      )
    }
  }
