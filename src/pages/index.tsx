import React from 'react'
import { Layout, Queue, History } from '../components'

export const IndexPage: React.FC = (props) => (
  <Layout>
    <h1>bitgate</h1>
    <section>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
        cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </p>
    </section>
    <div className="m1" />
    <section>
      <h2>job queue</h2>
      <Queue />
    </section>
    <div className="m1" />
    <section>
      <h2>job history</h2>
      <History />
    </section>
  </Layout>
)
