import { useState, useEffect } from 'react'

export function useLoader<T>(query: () => Promise<T>): [T | undefined, boolean, () => void] {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState<T | undefined>(undefined)
  const [counter, setCounter] = useState(0)

  useEffect(() => {
    setLoading(true)
    query().then((result) => {
      setData(result)
      setLoading(false)
    })
  }, [counter])

  return [data, loading, () => setCounter((c) => c + 1)]
}
