import { BaseAPIClient, APIClientConfig } from './BaseAPIClient'
import { AddJobFormData } from './types'

export class BridgeClient extends BaseAPIClient {
  constructor(config: APIClientConfig) {
    super(config)
  }

  public async getQueue() {
    const res = await this.axios.get('/queue')
    return res.data
  }

  public async insertJob(data: AddJobFormData) {
    const res = await this.axios.post('/queue', data)
    return res.data
  }

  public async getHistory() {
    const res = await this.axios.get('/history')
    return res.data
  }
}
