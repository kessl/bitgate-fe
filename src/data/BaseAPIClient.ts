import { default as Axios, AxiosInstance, AxiosRequestConfig } from 'axios'

export interface APIClientConfig {
  baseUrl: string
  injectAccessToken?: boolean
}

export class BaseAPIClient {
  public axios: AxiosInstance
  public readonly config: APIClientConfig

  constructor(config: APIClientConfig) {
    this.config = config
    this.axios = Axios.create({
      baseURL: config.baseUrl,
    })

    // this.initInterceptors()
  }

  // private initInterceptors() {
  //   this.axios.interceptors.request.use(this.injectAccessTokenRequestInterceptor)
  // }

  // injectAccessTokenRequestInterceptor = async (config: AxiosRequestConfig) => {
  //   const accessToken = ''

  //   if (accessToken && this.config.injectAccessToken) {
  //     config.headers.authorization = `Bearer ${accessToken}`
  //   }

  //   return config
  // }
}
