export interface Job {
  uid: string
  name: string
  position?: number
  code: string
  status: JobStatus
  result?: JobResult
}

export enum JobStatus {
  WAITING = 'WAITING',
  RUNNING = 'RUNNING',
  DONE = 'DONE',
  TIMED_OUT = 'TIMED_OUT',
}

export interface JobResult {
  executionDuration: number
  output: string
}

export interface AddJobFormData {
  name: string
  code: string
}
