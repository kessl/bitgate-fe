import { IndexPage } from './pages/index'
import { withRedux } from './store'

export default withRedux(IndexPage)
