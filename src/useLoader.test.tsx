import { renderHook, act } from '@testing-library/react-hooks'
import { useLoader } from './useLoader'

const wait = async (ms: number) => await new Promise((resolve) => setTimeout(resolve, ms))

describe('useLoader()', () => {
  it('updates loading state', async (done) => {
    const query = () => new Promise((resolve: any) => setTimeout(resolve([]), 50))
    const { result } = renderHook(() => useLoader(query))
    expect(result.current[1]).toEqual(true)
    await act(async () => {
      await wait(60)
    })
    expect(result.current[1]).toEqual(false)

    // refetch
    act(() => {
      result.current[2]()
    })
    expect(result.current[1]).toEqual(true)
    await act(async () => {
      await wait(60)
    })
    expect(result.current[1]).toEqual(false)
    done()
  })
})
