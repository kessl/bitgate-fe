const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const webpack = require('webpack')
const path = require('path')
const nodeExternals = require('webpack-node-externals')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const TerserJSPlugin = require('terser-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')

module.exports = function (env) {
  const serverConfig = {
    name: 'server',
    mode: 'production',
    devtool: 'source-map',
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    target: 'node',
    externals: [nodeExternals()],
    entry: ['./src/server/ssr.js'],
    output: {
      path: path.resolve(__dirname, '../../dist'),
      filename: 'server.bundle.js',
      library: 'Server',
      libraryTarget: 'commonjs2',
    },
    module: {
      rules: [
        {
          test: /\.(t|j)s(x?)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
            },
          ],
        },
        {
          enforce: 'pre',
          test: /\.(t|j)s(x?)$/,
          loader: 'source-map-loader',
        },
        {
          test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                modules: true,
                sourceMap: true,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
              },
            },
          ],
        },
        {
          test: /global\.css$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                modules: false,
                sourceMap: true,
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new ForkTsCheckerWebpackPlugin(),
      new webpack.DefinePlugin({
        API_BASE_URL: `"${env.API_BASE_URL || 'http://localhost:3001/v1'}"`,
      }),
      new MiniCssExtractPlugin({
        filename: 'public/[name].css',
      }),
    ],
    optimization: {
      minimizer: [
        new TerserJSPlugin({
          sourceMap: true,
        }),
        new OptimizeCSSAssetsPlugin({
          cssProcessorOptions: {
            map: {
              inline: true,
            },
          },
        }),
      ],
    },
  }

  if (env.NODE_ENV !== 'production') {
    serverConfig.mode = 'development'
  }

  return serverConfig
}
