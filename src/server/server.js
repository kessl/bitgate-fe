const webpack = require('webpack')
const clientConfig = require('../../webpack.config')
const serverConfig = require('./webpack.server.config')
const express = require('express')
const fs = require('fs')

const config = (env) => [clientConfig(env), serverConfig(env)]

const serverBundlePath = '../../dist/server.bundle.js'

// if building, compile and exit
if (process.env.BUILD) {
  const compiler = webpack(
    config({
      ...process.env,
      NODE_ENV: 'production',
    })
  )

  compiler.run((err, stats) => {
    if (err) console.error(err)
    stats.stats.forEach((s) => {
      console.log(`Built ${s.compilation.name} bundle in ${s.endTime - s.startTime} ms`)
    })
    process.exit(0)
  })
}

// run server
if (!process.env.BUILD) {
  const app = express()
  const compiler = webpack(config(process.env))
  const port = process.env.PORT || 3000

  // check whether bundle is built
  if (process.env.NODE_ENV === 'production') {
    try {
      fs.existsSync(serverBundlePath)
    } catch (err) {
      console.error('No build found. Run `yarn build` first.')
      process.exit(1)
    }
  }

  // run webpack in watch mode
  if (process.env.NODE_ENV !== 'production') {
    const watching = compiler.watch(
      {
        aggregateTimeout: 300,
        poll: undefined,
      },
      (err, stats) => {
        if (err) console.error(err)
        stats.stats.forEach((s) => {
          console.log(`Rebuilt ${s.compilation.name} bundle in ${s.endTime - s.startTime} ms`)
        })
      }
    )
  }

  app.get('/', (req, res) => {
    const html = require(serverBundlePath)
    res.end(html)
  })

  app.get('/healthcheck', (req, res) => {
    res.json({
      health: 'ok',
    })
  })

  app.use(express.static('public'), express.static('dist/public'))
  app.listen(port, () => console.log(`Server listening on port ${port}`))
}
