const React = require('react')
const App = require('../app').default
const { renderToString } = require('react-dom/server')

const html =
  '<!DOCTYPE html>' +
  renderToString(
    <html>
      <head>
        <meta charSet="UTF-8" />
        <title>bitgate</title>
        <link href="./main.css" rel="stylesheet" />
      </head>
      <body>
        <div id="root">
          <App />
        </div>
        <script src="./client.bundle.js"></script>
      </body>
    </html>
  )

module.exports = html
