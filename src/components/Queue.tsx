import React, { useState, useCallback } from 'react'
import { fetchQueue, useRootDispatch } from '../store'
import { useLoader } from '../useLoader'
import { Job } from './Job'
import css from './Queue.scss'
import { JobDetail } from '.'
import cn from 'classnames'
import { NewJob } from './NewJob'
import { Job as JobType } from '../data/types'

export const Queue: React.FC = (props) => {
  const query = useRootDispatch(fetchQueue)
  const [jobs, loading, refetch] = useLoader(query)
  const [selected, setSelected] = useState<string>(
    typeof window !== 'undefined' ? window.location.hash.replace('#', '') : ''
  )
  const selectedJob = !loading && jobs?.find((job) => job.uid === selected)

  const select = useCallback(
    (uid: string) => {
      setSelected(uid)
      window.location.hash = '#' + uid
    },
    [setSelected]
  )

  const onNewJobAdded = useCallback(
    (job: JobType) => {
      refetch()
      select(job.uid)
    },
    [refetch, select]
  )

  return (
    <>
      <div className={css.queue}>
        {loading ? (
          <>loading jobs...</>
        ) : (
          <>
            {jobs.map((job) => (
              <Job job={job} key={job.uid} selected={job.uid === selected} onClick={select} />
            ))}
            <div
              className={cn(css.newJob, selected === 'new' && css.selected)}
              onClick={() => select('new')}
            >
              <span>new job</span>
            </div>
          </>
        )}
      </div>
      <div className="m1-2" />
      {!loading && selected ? (
        selected !== 'new' ? (
          selectedJob && <JobDetail job={selectedJob} />
        ) : (
          <NewJob onSuccess={onNewJobAdded} />
        )
      ) : (
        <p className="m1-2">select a job to see details</p>
      )}
    </>
  )
}
