import React, { MouseEvent } from 'react'
import { useState } from 'react'

interface ButtonProps {
  onClick?: React.EventHandler<MouseEvent>
  loading?: boolean
  isFormSubmit?: boolean
  placeholder?: React.ReactNode
}

export const Button: React.FC<ButtonProps> = (props) => {
  const [loading, setLoading] = useState(false)

  const handleClick: React.EventHandler<MouseEvent> = async (e) => {
    if (loading || !props.onClick) {
      return
    }

    setLoading(true)
    await props.onClick(e)
    setLoading(false)
  }

  return (
    <button
      onClick={handleClick}
      disabled={loading || props.loading}
      type={props.isFormSubmit ? 'submit' : 'button'}
    >
      {loading || props.loading ? props.placeholder : props.children}
    </button>
  )
}

Button.defaultProps = {
  placeholder: <>loading...</>,
}
