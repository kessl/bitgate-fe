import React from 'react'
import css from './JobDetail.scss'
import { Job as JobType } from '../data/types'
import cn from 'classnames'

interface JobDetailProps {
  job: JobType
}

export const JobDetail: React.FC<JobDetailProps> = (props) => {
  return (
    <div className={css.jobDetail}>
      <p className={css.meta}>job {props.job.uid}</p>
      <p className={css.meta}>{props.job.name}</p>
      <p className={css.meta}>status {props.job.status}</p>
      {props.job.position && <p className={css.meta}>position in queue {props.job.position}</p>}
      {props.job.result && (
        <>
          <p className={cn(css.meta, 'm1-2')}>
            execution time {props.job.result.executionDuration}
          </p>
          <p className={css.meta}>output</p>
          <pre>{props.job.result.output}</pre>
        </>
      )}
      <div className="m1-2" />
      <p className={css.meta}>source code</p>
      <pre>{props.job.code}</pre>
    </div>
  )
}
