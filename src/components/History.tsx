import React, { useState, useCallback } from 'react'
import { useRootDispatch, fetchHistory } from '../store'
import { useLoader } from '../useLoader'
import { Job } from './Job'
import css from './History.scss'
import { JobDetail } from './JobDetail'

export const History: React.FC = (props) => {
  const query = useRootDispatch(fetchHistory)
  const [jobs, loading] = useLoader(query)
  const [selected, setSelected] = useState<string>(
    typeof window !== 'undefined' ? window.location.hash.replace('#', '') : ''
  )
  const selectedJob = !loading && jobs?.find((job) => job.uid === selected)

  const select = useCallback(
    (uid: string) => {
      setSelected(uid)
      window.location.hash = '#' + uid
    },
    [setSelected]
  )

  return (
    <>
      <div className={css.history}>
        {loading ? (
          <>loading jobs...</>
        ) : (
          jobs.map((job) => (
            <Job job={job} key={job.uid} selected={job.uid === selected} onClick={select} />
          ))
        )}
      </div>
      <div className="m1-2" />
      {!loading && selectedJob ? (
        <JobDetail job={selectedJob} />
      ) : (
        <p className="m1-2">select a job to see details</p>
      )}
    </>
  )
}
