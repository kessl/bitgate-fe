import React, { FormEventHandler, useState, ChangeEventHandler } from 'react'
import css from './NewJob.scss'
import { useRootDispatch, addJob } from '../store'
import { Button } from './Button'
import { Job } from '../data/types'

interface NewJobProps {
  onSuccess: (job: Job) => void
}

export const NewJob: React.FC<NewJobProps> = (props) => {
  const [state, setState] = useState({
    name: '',
    code: '',
  })
  const [submitting, setSubmitting] = useState(false)

  const handleChange: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement> = (e) => {
    const { name, value } = e.target
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }))
  }

  const submit = useRootDispatch(addJob)
  const handleSubmit: FormEventHandler = async (e) => {
    e.preventDefault()
    setSubmitting(true)
    const res = await submit(state)
    props.onSuccess(res)
  }

  return (
    <form
      className={css.newJob}
      action={`${process.env.API_BASE_URL}/queue`}
      method="post"
      onSubmit={handleSubmit}
    >
      <div className={css.row}>
        <div className={css.cell}>job name</div>
        <div className={css.cell}>
          <input name="name" type="text" value={state.name} onChange={handleChange} />
        </div>
      </div>
      <div className={css.row}>
        <div className={css.cell}>job status</div>
        <div className={css.cell}>NEW</div>
      </div>
      <div className={css.row}>
        <div className={css.cell}>position in queue</div>
        <div className={css.cell}>last</div>
      </div>
      <div className={css.row}>
        <div className={css.cell}>code</div>
        <textarea className={css.code} name="code" value={state.code} onChange={handleChange} />
      </div>
      <div className={css.row}>
        <div className={css.cell}></div>
        <div className={css.cell}>
          <Button isFormSubmit loading={submitting} placeholder="saving...">
            add job
          </Button>
        </div>
      </div>
    </form>
  )
}
