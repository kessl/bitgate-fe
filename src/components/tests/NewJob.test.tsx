import React from 'react'
import { mount } from 'enzyme'
import { NewJob } from '..'

jest.mock('react-redux', () => ({
  useDispatch: () => () => ({}),
}))

const wait = async (ms: number) => await new Promise((resolve) => setTimeout(resolve, ms))

describe('<NewJob />', () => {
  it('calls onSuccess after submit', async (done) => {
    const cb = jest.fn()
    const newJob = mount(<NewJob onSuccess={cb} />)
    expect(cb).toHaveBeenCalledTimes(0)
    newJob.find('form').simulate('submit')
    await wait(10)
    expect(cb).toHaveBeenCalledTimes(1)
    expect(cb).toHaveBeenCalledWith({})
    done()
  })

  it('handles input', () => {
    const newJob = mount(<NewJob onSuccess={() => undefined} />)
    newJob.find('input').simulate('change', { target: { name: 'name', value: 't e s t' } })
    newJob.update()
    expect(newJob.find('input').props().value).toEqual('t e s t')
  })
})
