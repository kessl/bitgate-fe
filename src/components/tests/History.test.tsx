import React from 'react'
import { mount } from 'enzyme'
import { History } from '..'
import { JobStatus } from '../../data/types'
import { act } from 'react-dom/test-utils'

jest.mock('react-redux', () => ({
  useDispatch: () => () => ({
    uid: 'testuid',
  }),
}))

jest.mock('../../useLoader', () => ({
  useLoader: () => [
    [
      {
        uid: 'testuid',
        name: '',
        code: '',
        status: JobStatus.DONE,
        position: 1,
      },
    ],
    false,
    (): void => undefined,
  ],
}))

describe('<Queue />', () => {
  it('selects a job', () => {
    const queue = mount(<History />)
    queue.find('.job').simulate('click')
    expect(queue.find('.job').hasClass('selected'))
  })
})
