import React from 'react'
import { shallow } from 'enzyme'
import { Job } from '..'
import { JobStatus } from '../../data/types'

const mockJob = {
  uid: '',
  name: '',
  code: '',
  status: JobStatus.WAITING,
  position: 1,
}

describe('<Job />', () => {
  it('calls onClick', () => {
    const cb = jest.fn()
    const job = shallow(<Job job={mockJob} onClick={cb} selected />)
    expect(cb).toHaveBeenCalledTimes(0)
    job.find('div').simulate('click')
    expect(cb).toHaveBeenCalledTimes(1)
  })
})
