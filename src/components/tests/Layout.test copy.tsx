import React from 'react'
import { shallow, mount } from 'enzyme'
import { Layout } from '..'

describe('<Layout />', () => {
  it('renders children', () => {
    const child = <div className="fancy-div" />
    const layout = shallow(<Layout>{child}</Layout>)
    expect(layout.contains(child)).toEqual(true)
  })

  it('renders main tag', () => {
    const layout = shallow(<Layout />)
    expect(layout.contains(<main className="layout" />)).toEqual(true)
  })

  it('updates title', () => {
    mount(<Layout title="test title" />)
    expect(document.title).toEqual('test title')
  })

  it('does not change title if not set', () => {
    document.title = 'fancy test title'
    mount(<Layout />)
    expect(document.title).toEqual('fancy test title')
  })
})
