import React from 'react'
import { shallow } from 'enzyme'
import { Button } from '..'

const wait = async (ms: number) => await new Promise((resolve) => setTimeout(resolve, ms))

describe('<Button />', () => {
  it('renders text node', () => {
    const text = 'button text'
    const button = shallow(<Button>{text}</Button>)
    expect(button.text()).toEqual(text)
  })

  it('renders submit button', () => {
    const button = shallow(<Button isFormSubmit />)
    expect(button.find('button').prop('type')).toEqual('submit')
  })

  it('renders loading placeholder', async (done) => {
    const cb = jest.fn(async () => new Promise((resolve) => setTimeout(resolve, 20)))
    const button = shallow(
      <Button placeholder="loading" onClick={cb}>
        test
      </Button>
    )
    button.find('button').simulate('click')
    expect(button.text()).toEqual('loading')
    button.find('button').simulate('click')
    await wait(30)
    expect(button.text()).toEqual('test')
    done()
  })
})
