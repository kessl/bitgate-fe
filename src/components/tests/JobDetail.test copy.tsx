import React from 'react'
import { shallow } from 'enzyme'
import { JobDetail } from '..'
import { JobStatus } from '../../data/types'

const mockJob = {
  uid: '',
  name: '',
  code: '',
  status: JobStatus.WAITING,
  position: 1,
  result: {
    executionDuration: 1,
    output: '',
  },
}

describe('<JobDetail />', () => {
  it('renders', () => {
    const jobDetail = shallow(<JobDetail job={mockJob} />)
    expect(jobDetail).not.toBeUndefined()
  })
})
