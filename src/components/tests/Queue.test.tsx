import React from 'react'
import { mount } from 'enzyme'
import { Queue } from '..'
import { JobStatus } from '../../data/types'
import { act } from 'react-dom/test-utils'

jest.mock('react-redux', () => ({
  useDispatch: () => () => ({
    uid: 'testuid',
  }),
}))

jest.mock('../../useLoader', () => ({
  useLoader: () => [
    [
      {
        uid: 'testuid',
        name: '',
        code: '',
        status: JobStatus.WAITING,
        position: 1,
      },
    ],
    false,
    (): void => undefined,
  ],
}))

const wait = async (ms: number) => await new Promise((resolve) => setTimeout(resolve, ms))

describe('<Queue />', () => {
  it('selects a job', () => {
    const queue = mount(<Queue />)
    queue.find('.job').simulate('click')
    expect(queue.find('.job').hasClass('selected'))
  })

  it('opens new job form, selects added job', async (done) => {
    const queue = mount(<Queue />)
    queue.find('.newJob').simulate('click')
    expect(queue.find('form').hasClass('newJob')).toEqual(true)
    await act(async () => {
      queue.find('form').simulate('submit')
      await wait(50)
    })
    queue.update()
    expect(queue.find('.job').hasClass('selected')).toEqual(true)
    done()
  })
})
