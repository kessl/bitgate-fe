import React, { useEffect } from 'react'
import '../global.css'
import css from './Layout.scss'

interface LayoutProps {
  title?: string
}

export const Layout: React.FC<LayoutProps> = (props) => {
  useEffect(() => {
    if (props.title !== undefined) {
      document.title = props.title
    }
  }, [props.title])

  return (
    <>
      <main className={css.layout}>{props.children}</main>
      <footer className={css.footer}>
        <a href="https://bitgate.cz">bitgate.cz</a>
      </footer>
    </>
  )
}
