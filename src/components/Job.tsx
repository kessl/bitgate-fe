import React, { MouseEventHandler, useCallback } from 'react'
import css from './Job.scss'
import { Job as JobType, JobStatus } from '../data/types'
import cn from 'classnames'

interface JobProps {
  job: JobType
  selected?: boolean
  onClick?: (uid: string) => void
}

export const Job: React.FC<JobProps> = (props) => {
  const handleClick: MouseEventHandler = useCallback(
    (e) => {
      props.onClick && props.onClick(props.job.uid)
    },
    [props.job, props.onClick]
  )

  return (
    <div className={cn(css.job, props.selected && css.selected)} onClick={handleClick}>
      <p className={css.meta}>job {props.job.uid}</p>
      <p className={css.meta}>{props.job.name}</p>
      <p className={css.meta}>status {props.job.status}</p>
      {props.job.position && <p className={css.meta}>position {props.job.position}</p>}
    </div>
  )
}
