# bitgate

React front-end to the bitgate API. The app shows current and past jobs and allows users to queue new jobs to be run on a 8-bit breadboard CPU running in my living room.

[![pipeline status](https://gitlab.com/kessl/bitgate-fe/badges/master/pipeline.svg)](https://gitlab.com/kessl/bitgate-fe/pipelines)
[![coverage report](https://gitlab.com/kessl/bitgate-fe/badges/master/coverage.svg)](https://bitgate.cz/coverage)
