FROM node:12.12-alpine

ENV NODE_ENV production
ENV PORT 3000
EXPOSE 3000

RUN mkdir -p /opt/app && apk add --no-cache curl
HEALTHCHECK --interval=1s --timeout=3s --retries=3 CMD curl --fail http://localhost:3000/healthcheck || exit 1  

USER node
CMD ["npm", "start"]

WORKDIR /opt/app
COPY . /opt/app
