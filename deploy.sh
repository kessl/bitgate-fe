#!/bin/bash
set -e
docker pull $1:$2
docker stop $1 || true
docker rm $1 || true
docker run -dt -p 80:3000 --restart=always $1:$2
